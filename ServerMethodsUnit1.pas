unit ServerMethodsUnit1;

interface

uses System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth;

type
{$METHODINFO ON}
  TServerMethods1 = class(TComponent)
  private
    FNumeroNegativo: boolean;
    property NumeroNegativo: boolean read FNumeroNegativo write FNumeroNegativo;
    { Private declarations }
  public
    { Public declarations }
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
    function Extenso(Valor : integer): String;
  end;
{$METHODINFO OFF}

implementation

uses System.StrUtils, uConstantes;

function TServerMethods1.EchoString(Value: string): string;
begin
  Result := Value;
end;

function TServerMethods1.Extenso(Valor: integer): String;
var
  Centena, Milhar, Milhao, Bilhao, Texto : string;
function ConcatenaValor( Valor: ShortString ): string;
var
    Unidade, Dezena, Centena: String;
  begin
    if (Valor[2] = '1') and (Valor[3] <> '0') then
    begin
      Unidade := Dez[StrToInt(Valor[3])];
      Dezena := '';
    end
    else
    begin
     if Valor[2] <> '0' then
       Dezena := Dezenas[StrToInt(Valor[2])];
     if Valor[3] <> '0' then
       unidade := Unidades[StrToInt(Valor[3])];
    end;
    if (Valor[1] = '1') and (Unidade = '') and (Dezena = '') then
      centena := 'cem'
    else
      if Valor[1] <> '0' then
        Centena := Centenas[StrToInt(Valor[1])]
      else
        Centena := '';

    Result := Centena + IfThen( (Centena <> '') and ((Dezena <> '') or
    (Unidade <> '')),' e ', '') + Dezena + IfThen( (Dezena <> '') and
    (Unidade <> ''), ' e ','') + Unidade;
  end;

begin
  NumeroNegativo := Copy(IntToStr(Valor), 1, 1) = '-';

  if NumeroNegativo then
    Valor := not Valor;
  try
    StrToInt(IntToStr(Valor));
  except
    on E:Exception do
      raise Exception.Create('Erro ao converter n�mero para extenso.');
  end;
  if ((Valor > 99999) or (valor < (not 99999))) then
  begin
   Result := 'Valor fora da faixa dispon�vel [-99999, 99999]';
   Exit;
  end;

  if Valor = 0 then
  begin
    Result := 'zero';
    Exit;
  end;

  Texto := FormatFloat( '000000000000.00', Valor );
  Centena := ConcatenaValor( Copy( Texto, 10, 3 ) );
  Milhar := ConcatenaValor( Copy( Texto,  7, 3 ) );

  if Milhar <> '' then
    Milhar := Milhar + ' mil';

  Milhao := ConcatenaValor( Copy( Texto,  4, 3 ) );

  if Milhao <> '' then
    Milhao := Milhao + IfThen( Copy( Texto, 4, 3 ) = '001', ' milh�o', ' milh�es');

  Bilhao := ConcatenaValor( Copy( Texto,  1, 3 ) );

  if Bilhao <> '' then
    Bilhao := Bilhao + IfThen( Copy( Texto, 1, 3 ) = '001', ' bilh�o', ' bilh�es');

  Result := Bilhao + IfThen((Bilhao <> '') and (Milhao + Milhar + Centena <> ''),
    IfThen((Pos(' e ', Bilhao) > 0) or (Pos( ' e ',
    Milhao + Milhar + Centena ) > 0), ', ', ' e '), '') +
    Milhao + IfThen( (Milhao <> '') and (Milhar + Centena <> ''),
    IfThen((Pos(' e ', Milhao) > 0) or (Pos( ' e ', Milhar + Centena ) > 0 ),', ',    ' e '), '') +
            Milhar + IfThen( (Milhar <> '') and
    (Centena <> ''), IfThen(Pos( ' e ', Centena ) > 0, ', ', ' e '),'') + Centena;

  if NumeroNegativo then
    Result := 'menos ' + Result;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
  Result := System.StrUtils.ReverseString(Value);
end;
end.

